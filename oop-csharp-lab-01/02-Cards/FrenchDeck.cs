﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;

        public FrenchDeck()
        {
            cards = new Card[Enum.GetNames(typeof(FrenchSeed)).Length * Enum.GetNames(typeof(FrenchValue)).Length + Enum.GetNames(typeof(FrenchJolly)).Length];
        }

        public Card this[FrenchSeed seed, FrenchValue value]
        {
            get
            {
                int k = 0;

                foreach (FrenchSeed s in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
                {
                    if (s.Equals(seed))
                    {
                        foreach (FrenchValue v in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                        {
                            if (v.Equals(value))
                            {
                                return cards[k];
                            }
                            k++;
                        }
                    }
                }
                return cards[k];
            }
        }

        public Card this[FrenchJolly jolly]
        {

            get
            {
                int x = 0;
                foreach (FrenchJolly j in (FrenchJolly[])Enum.GetValues(typeof(FrenchJolly)))
                {
                    if (j.Equals(jolly))
                    {
                        return cards[x];
                    }
                    x++;
                }
                return cards[x];
            }

        }

        public  void Initialize()
        {
            int i = 0;

            foreach (FrenchSeed seed in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue value in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    cards[i] = new Card(value.ToString(), seed.ToString());
                    i++;
                }
            }
            int j = 0;
            foreach(FrenchJolly jolly in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
            {
                cards[j] = new Card(jolly.ToString(), "jolly");
                j++;
            }

        }



        public void Print()
        {
            foreach (Card i in cards)
            {
                Console.WriteLine(i);
            }
        }

    }
    enum FrenchSeed
    {
        CUORI,
        QUADRI,
        FIORI,
        PICCHE
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        DIECI,
        JACK,
        QUEEN,
        KING
        
    }

    enum FrenchJolly
    {
        RED,
        BLACK
    }
}
